﻿using System;

using Xamarin.Forms;
using System.Diagnostics;

namespace TESTAPP
{
    public class App : Application
    {
        public App()
        {
            var web = new WebView()
            {
                Source = new UrlWebViewSource(){ Url = "https://demo.docusign.net/Signing/startinsession.aspx?t=973cc953-82ea-46f3-9e57-688bbfe8a861" },
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            // The root page of your application
            MainPage = new ContentPage
            {
                Content = new StackLayout
                {
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    Padding = new Thickness(0, 20F, 0, 0),
                    Children =
                    {
                        web
                    }
                }
            };
        }
    }
}

